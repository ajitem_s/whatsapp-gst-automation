"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Constants {
}
Constants.chatButtonSelector = '#side .rAUz7 div[role=button] span[data-icon=chat]';
Constants.searchResultSelector = '._2vPAk .RLfQR ._2wP_Y';
Constants.messageBoxSelector = '#main ._2S1VP.copyable-text.selectable-text';
Constants.attachButtonSelector = '#main ._3AwwN ._1i0-u .rAUz7 div[title=Attach]';
Constants.attachInputSelector = '#main ._3AwwN ._1i0-u .rAUz7 ._3s1D4 input[type=file]';
exports.Constants = Constants;
//# sourceMappingURL=constants.js.map