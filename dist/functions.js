"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function nodeAppears(client, selector, timeout = 30000) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            const { Runtime } = client;
            let counter = 0;
            const timerId = yield setInterval(() => __awaiter(this, void 0, void 0, function* () {
                counter += 1000;
                if (counter >= timeout) {
                    clearInterval(timerId);
                    return reject(`Unable to find node pointed by ${selector}`);
                }
                const { result: { value } } = yield Runtime.evaluate({
                    expression: `document.querySelector('${selector}')`
                }).catch(() => (console.log('')));
                if (value !== null) {
                    clearInterval(timerId);
                    return resolve(value);
                }
            }), 1000);
        })).catch(() => (console.log('')));
    });
}
exports.nodeAppears = nodeAppears;
//# sourceMappingURL=functions.js.map