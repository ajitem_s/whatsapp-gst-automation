"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const chromeLauncher = require("chrome-launcher");
class Routes {
    routes(app) {
        app.route('/').get((req, res) => {
            res.status(200).send({
                message: 'GET request successfulll!!!!'
            });
        });
        app.route('/start').get((req, res) => {
            chromeLauncher.launch({
                chromeFlags: [
                    '--remote-debugging-address=0.0.0.0',
                    '--no-sandbox',
                    '--disable-popup-blocking',
                    '--ignore-certificate-errors',
                    '--enable-features=NetworkService',
                    '--headless'
                ],
                chromePath: "/usr/bin/google-chrome-stable",
                userDataDir: __dirname + '../chrome-data'
            }).then((chrome) => __awaiter(this, void 0, void 0, function* () {
                res.status(200).send({
                    message: 'chrome launched successfully',
                    port: chrome.port,
                    pid: chrome.pid
                });
            })).catch((error) => __awaiter(this, void 0, void 0, function* () {
                res.status(200).send({
                    message: 'unable to launch chrome',
                });
            }));
        });
        app.route('/stop').post((req, res) => {
            const { processId } = req.body;
            if (processId) {
                if (process.kill(processId, 0)) {
                    process.kill(processId);
                    res.status(200).send({
                        message: `chrome processed stopped`
                    });
                }
                else {
                    res.status(422).send({
                        message: `process with id ${processId} does not exist`
                    });
                }
            }
            else {
                res.status(422).send({
                    message: `process id is a mandatory field`
                });
            }
        });
    }
}
exports.Routes = Routes;
//# sourceMappingURL=routes.js.map