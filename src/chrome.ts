import * as CDP from 'chrome-remote-interface';
import * as chromeLauncher from 'chrome-launcher';
import {Constants} from "./constants";
import {nodeAppears} from "./functions";

chromeLauncher.launch({
    chromeFlags: [
        '--remote-debugging-address=0.0.0.0',
        '--no-sandbox',
        '--disable-popup-blocking',
        '--ignore-certificate-errors',
        '--enable-features=NetworkService'
    ],
    chromePath: "/usr/bin/google-chrome-stable",
    userDataDir: '/home/ajitem/Projects/whatsapp-poc-chrome/chrome-data'
}).then(async (chrome) => {
    console.log(`Chrome started on port ${chrome.port}`);
    const host = 'localhost';
    const port = chrome.port;
    // open new tab
    const tab = await CDP.New({host, port});

    // initialize client
    const client = await CDP({host, port, tab});
    // extract domains
    const {Page, Runtime, DOM} = client;

    await Page.enable();
    console.log(`Open WhatsApp!`);
    await Page.navigate({url: 'https://web.whatsapp.com'});
    await Page.loadEventFired();
    // page opened
    console.log(`Waiting for WhatsApp to load`);

    await nodeAppears(client, Constants.chatButtonSelector);

    console.log('WhatsApp loaded!');

    const names = ['Amruta Bafna'];//, 'Vaby Wilson', 'Paresh Bafna',
    // 'Amruta Bafna', 'Wasim Shaikh', 'Pratik Thorat', 'Afzal GSTHero',
    // 'Abhishek Bhale'];

    for (const name of names) {
        // search contact
        await Runtime.evaluate({
            expression: `
        // create mouse down event
        var mouseDownEvent = document.createEvent('MouseEvents');
        mouseDownEvent.initEvent('mousedown', true, true);
        console.log('event created');
        
        // find chat button
        var chatButton = document.querySelector('${Constants.chatButtonSelector}').parentNode
        
        // click on it
        chatButton.dispatchEvent(mouseDownEvent);
        console.log('clicked');
        
        // create focus event
        var focusEvent = new Event('UIEvent');
        focusEvent.initEvent('focus', false, true);
        
        // find search box
        document.querySelectorAll('input.jN-F5')[0].dispatchEvent(focusEvent);
        
        // create special value setter (react!!)
        document.querySelectorAll('input.jN-F5')[0].dispatchEvent(focusEvent);
        nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
        // input name of contact
        nativeInputValueSetter.call(document.querySelectorAll('input.jN-F5')[0], '${name}');
        // create input event (react looks for input event to trigger onChange
        var inputEvent = new Event('input', { bubbles: true});
        
        // dispatch change event
        document.querySelectorAll('input.jN-F5')[0].dispatchEvent(inputEvent);
        console.log('search done');
    `
        });

        console.log(`Waiting for search result`);

        // await nodeAppears(client, Constants.searchResultSelector);
        await sleep(2000);

        console.log(`Searching done`);

        await Runtime.evaluate({
            expression: `
        console.log('find search result');
        // find chats and contacts (search result)
        var chatsAndContacts = document.querySelectorAll('${Constants.searchResultSelector}');
        for(i = 0; i < chatsAndContacts.length; i++) {
            if(chatsAndContacts[i].querySelector('span._3TEwt').innerText.trim() === '${name}') {
                console.log('contact found!');
                chatsAndContacts[i].querySelector('span._3TEwt').click();
                console.log('clicked on contact');
                break;
            }
        }
    `
        });

        await nodeAppears(client, Constants.messageBoxSelector);

        await Runtime.evaluate({
            expression: `
        var textbox = document.querySelector('${Constants.messageBoxSelector}');
        console.log('typing message...');

        for (i = 0; i < 1; i++) {
            textbox.textContent = "Hi ${name}. Please check out this book. It is awesome!";
            textbox.dispatchEvent(inputEvent);

            document.querySelector("#main ._35EW6 span").click();
        }
        console.log('message sent...');
        
        var attachButton = document.querySelector('#main ._3AwwN ._1i0-u .rAUz7 div[title=Attach]');
        var mouseDownEvent1 = document.createEvent('MouseEvents');
        mouseDownEvent1.initEvent('mousedown', true, true);
        attachButton.dispatchEvent(mouseDownEvent1);
    `
        });

        const {root} = await DOM.getDocument();
        const result = await DOM.querySelectorAll({
            nodeId: root.nodeId,
            selector: Constants.attachInputSelector
        });
        console.log("Result", result);
        // fill the file input
        // await DOM.setFileInputFiles({
        //     nodeId: result.nodeId[0],
        //     files: ['/home/ajitem/Projects/whatsapp-poc-chrome/images/test.jpeg']
        // });
        await DOM.setFileInputFiles({
            nodeId: result.nodeIds[1],
            files: ['/home/ajitem/Downloads/Greg_Kroah-Hartman_-_Linux_Kernel_in_a_Nutshell.pdf']
        });
        console.log('File Attached');

        // await nodeAppears(client, Constants.messageBoxSelector); // only for image
        await sleep(2000); // for file

        Runtime.evaluate({
            expression: `
        // var textbox2 = document.querySelector('._2vPAk ._2S1VP.copyable-text.selectable-text');
        //
        // textbox2.textContent = "Hi ${name}. Please check out this book. It is awesome!";
        // textbox2.dispatchEvent(inputEvent);
        
        console.log('sending file');

        document.querySelector("._2vPAk ._3nfoJ span").click();
        
        console.log('image + message sent...');
    `
        }).then(a => console.log(a)).catch(e => console.log(e));
    }
}).catch(error => console.log(error));

async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}