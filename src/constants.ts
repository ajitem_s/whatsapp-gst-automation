export class Constants {
    static chatButtonSelector = '#side .rAUz7 div[role=button] span[data-icon=chat]';
    static searchResultSelector = '._2vPAk .RLfQR ._2wP_Y';
    static messageBoxSelector = '#main ._2S1VP.copyable-text.selectable-text';
    static attachButtonSelector = '#main ._3AwwN ._1i0-u .rAUz7 div[title=Attach]';
    static attachInputSelector = '#main ._3AwwN ._1i0-u .rAUz7 ._3s1D4 input[type=file]';
}