export async function nodeAppears(client, selector: string, timeout: number = 30000) {
    return new Promise(async (resolve, reject) => {
        const {Runtime} = client;
        let counter = 0;
        const timerId = await setInterval(async () => {
            counter += 1000;

            if (counter >= timeout ) {
                clearInterval(timerId);
                return reject(`Unable to find node pointed by ${selector}`);
            }

            const {result: {value}} = await Runtime.evaluate({
                expression: `document.querySelector('${selector}')`
            }).catch(() => (console.log('')));

            if (value !== null) {
                clearInterval(timerId);
                return resolve(value);
            }
        }, 1000);
    }).catch(() => (console.log('')));
}