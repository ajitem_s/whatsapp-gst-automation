import {Application, Request, Response} from 'express';
import * as chromeLauncher from "chrome-launcher";

export class Routes {
    public routes(app: Application): void {
        app.route('/').get((req: Request, res: Response) => {
            res.status(200).send({
                message: 'GET request successfulll!!!!'
            })
        });

        app.route('/start').get((req: Request, res: Response) => {
            chromeLauncher.launch({
                chromeFlags: [
                    '--remote-debugging-address=0.0.0.0',
                    '--no-sandbox',
                    '--disable-popup-blocking',
                    '--ignore-certificate-errors',
                    '--enable-features=NetworkService',
                    '--headless'
                ],
                chromePath: "/usr/bin/google-chrome-stable",
                userDataDir: __dirname + '../chrome-data'
            }).then(async chrome => {
                res.status(200).send({
                    message: 'chrome launched successfully',
                    port: chrome.port,
                    pid: chrome.pid
                })
            }).catch(async error => {
                res.status(200).send({
                    message: 'unable to launch chrome',
                })
            });
        });

        app.route('/stop').post((req: Request, res: Response) => {
            const {processId} = req.body;

            if (processId) {
                if (process.kill(processId, 0)) {
                    process.kill(processId);
                    res.status(200).send({
                        message: `chrome processed stopped`
                    });
                } else {
                    res.status(422).send({
                        message: `process with id ${processId} does not exist`
                    });
                }
            } else {
                res.status(422).send({
                    message: `process id is a mandatory field`
                });
            }
        });
    }
}